require "rails_helper"

DEFAULT_CRONUTS_URL='https://upload.wikimedia.org/wikipedia/commons/8/8b/April_2016_Cronut%C2%AE_2_Burnt_Vanilla_Caramel_-_photo_by_Dominique_Ansel_Bakery.jpg'
DEFAULT_DONUTS_URL='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Glazed-Donut.jpg/250px-Glazed-Donut.jpg'

RSpec.feature "Cronuts Donuts URL", :type => :feature do
  scenario "without custom cronuts_url and donuts_url" do
    visit welcome_index_path

    expect(page).to have_link(href: DEFAULT_CRONUTS_URL)
    expect(page).to have_link(href: DEFAULT_DONUTS_URL)
  end

  scenario "with custom cronuts_url" do
    visit welcome_index_path(cronuts_url: 'https://google.com')

    expect(page).to have_link(href: 'https://google.com')
    expect(page).to have_link(href: DEFAULT_DONUTS_URL)
  end

  scenario "with custom donuts_url" do
    visit welcome_index_path(donuts_url: 'https://google.com')

    expect(page).to have_link(href: DEFAULT_CRONUTS_URL)
    expect(page).to have_link(href: 'https://google.com')
  end

  scenario "with custom both cronuts_url and donuts_url" do
    visit welcome_index_path(cronuts_url: 'https://google.com', donuts_url: 'https://google.com')

    expect(page).to have_link(href: 'https://google.com')
    expect(page).to have_link(href: 'https://google.com')
  end
end